### Resumo

<!-- Resuma o bug encontrado de forma concisa. -->

### Passos para reproduzir

<!-- Descreva como se pode reproduzir o problema - isso é muito importante. Por favor, use uma lista ordenada. -->

### Qual é o comportamento atual do **bug**?

<!-- Descreva o que realmente acontece. -->

### Qual é o comportamento **correto** esperado?

<!-- Descreva o que você deve ver em vez disso. -->

### Qual é a versão

<!-- Descreva em qual versão do projeto o bug está acontecendo. -->

### Logs e/ou capturas de tela relevantes

<!-- Cole todos os logs relevantes - use code blocks (```) para formatar a saída do console, logs, e código, pois é difícil de ler de outra forma. -->

### Possíveis correções

<!-- Se puder, link para a linha de código que pode ser responsável pelo problema. -->

/label ~"type::bug"
